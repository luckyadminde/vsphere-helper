﻿Function VSphere-Report {
<#
.SYNOPSIS
    Display easy readable informations from esxi/vcenter

    *** Needs VMWare.PowerCLI loaded and connected to esxi/vcenter
    Further Help can be found with 'Get-Help VSphere-Report -Examples'
.DESCRIPTION
    Use VSphere-Report to Display information in console or export as csv-file.
.EXAMPLE
#
To display Datastores VMs running on use:
    Vsphere-Report -Server <Connection Name of PowerCLI> -Datastores -VM <VM Name or * for all VMs> 
    
Quering all VMs can take long time, depending on VM Count.
.EXAMPLE
#
To display Snapshots on ESXi Host or VCenter use:
    Vsphere-Report -Server <Connection Name of PowerCLI> -Snapshots -VM <VM Name or * for all VMs> 
    
Quering all VMs can take long time, depending on VM Count.
.EXAMPLE 
#
To display VMs that need snapshot-consolidation use:
    Vsphere-Report -Server <Connection Name of PowerCLI> -Consolidation -VM <VM Name or * for all VMs> 
    
Quering all VMs can take long time, depending on VM Count.
.EXAMPLE
#
To display average CPU usage (usage in %, ready in ms, ready in %) of VMs use:
    Vsphere-Report -Server <Connection Name of PowerCLI> -Ressource CPU -Intervall <Day,Week,Month> -VM <VM Name or * for all VMs>
-Intervall specifies average CPU usage since a day, week or month.

Quering all VMs can take long time, depending on VM Count.
#>
    param(
        [parameter(ParameterSetName = 'DataStores', Mandatory = $true, Position = '3')]
        [Switch]$Datastores,
        [parameter(ParameterSetName = 'Snapshots', Mandatory = $true, Position = '3')]
        [Switch]$Snapshots,
        [parameter(ParameterSetName = 'Consolidation', Mandatory = $true, Position = '3')]
        [Switch]$Consolidation,
        [Parameter(ParameterSetName = 'Monitor', Mandatory = $true, Position = '3')]
        [Alias('Ressource')]
        [ValidateSet('CPU', 'HDD')]
        [String]$Monitor,
        [Parameter(ParameterSetName = 'Monitor', Mandatory = $true, Position = '4')]
        [ValidateSet('Day', 'Week', 'Month')]
        [String]$Intervall,
        [parameter(Mandatory = $true, Position = '1')]
        [IPAddress]$Server,
        [parameter(Mandatory = $true, Position = '2')]
        [String]$VM,
        [parameter(Mandatory = $false, Position = '5')]
        [String]$ReportPath
    )

    if ($Datastores) {
        if ($ReportPath) {
            Report-Datastores -Server $Server -VM $VM -ReportPath $ReportPath
        }
        if (!($ReportPath)) {
            Report-DataStores -Server $Server -VM $VM
        }
    }

    if ($Snapshots) {
        if ($ReportPath) {
            Report-Snapshots -Server $Server -VM $VM -ReportPath $ReportPath
        }
        if (!($ReportPath)) {
            Report-Snapshots -Server $Server -VM $VM
        }
    }

    if ($Consolidation) {
        if ($ReportPath) {
            Report-Consolidation -Server $Server -VM $VM -ReportPath $ReportPath
        }
        if (!($ReportPath)) {
            Report-Consolidation -Server $Server -VM $VM
        }
    }

    if ($Monitor) {
        if ($ReportPath) {
            Report-VmStats -Server $Server -VM $VM -Ressource $Monitor -Intervall $Intervall -ReportPath $ReportPath
        }
        if (!($ReportPath)) {
            Report-VmStats -Server $Server -VM $VM -Ressource $Monitor -Intervall $Intervall
        }
    }





}
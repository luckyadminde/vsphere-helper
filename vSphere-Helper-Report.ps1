﻿Function Report-Datastores{
param(
    [parameter(Mandatory=$true)][String]$Server,
    [parameter(Mandatory=$true)][String]$VM,
    [Parameter(Mandatory=$false)][String]$ReportPath
    )

    $dataStores = Get-AllVmDataStores -Server $Server -VM $VM
    $freeSpaceGB = @{Name = 'FreeSpaceGB';Expression={'{0:0}' -f ($_.FreeSpaceGB)}}
    $capacityGB = @{Name = 'CapacityGB';Expression={'{0:0}' -f ($_.CapacityGB)}}
    $dataStoresFormatted = @()

    foreach($dataStore in $dataStores){
        $dataStoresFormatted += $dataStore | Select-Object Name,$freeSpaceGB,$capacityGB
    }
    if($ReportPath){
        $dataStoresFormatted | ConvertTo-Csv -NoTypeInformation | Out-File $ReportPath
    }
     if(!$ReportPath){
        $dataStoresFormatted
    }

}

Function Report-Snapshots{
param(
    [parameter(Mandatory=$true)][String]$Server,
    [parameter(Mandatory=$true)][String]$VM,
    [Parameter(Mandatory=$false)][String]$ReportPath
    )

    $sizeGB = @{Name = 'SizeGB';Expression={'{0:0.0}' -f ($_.SizeGB)}}
    $createdDate = @{Name = 'CreationDate';Expression={$_.Created.ToString("dd.MM.yyyy")}}
    $snapshots = Get-Snapshot -VM $VM | Select-Object VM,Name,$sizeGB,$createdDate

    if($ReportPath){
        $snapshots | ConvertTo-Csv -NoTypeInformation | Out-File $ReportPath
    }
     if(!$ReportPath){
        $snapshots
    }
}

Function Report-Consolidation{
param(
    [parameter(Mandatory=$true)][String]$Server,
    [parameter(Mandatory=$true)][String]$VM,
    [parameter(Mandatory=$false)][String]$ReportPath
    )

    $vmObjects = Get-VM $VM
    $consolidationVm = $vmObjects | Where-Object {$_.ExtensionData.Runtime.consolidationNeeded} | Select-Object Name

    if($ReportPath){
        $consolidationVm | ConvertTo-Csv -NoTypeInformation | Out-File $ReportPath
    }
     if(!$ReportPath){
        $consolidationVm
    }
}



Function Report-VmStats{
    param(
    [parameter(Mandatory=$true)][String]$Server,
    [parameter(Mandatory=$true)][String]$VM,
    [Parameter(Mandatory=$true)][ValidateSet('CPU')][String]$Ressource,   
    [Parameter(Mandatory=$true)][ValidateSet('Day','Week','Month')][String]$Intervall,
    [parameter(Mandatory=$false)][String]$ReportPath
    )
    Switch ($Intervall){
        'Day' {$days=-1;$mins=5;$divider=3000}
        'Week' {$days=-7;$mins=30;$divider=18000}
        'Month' {$days=-30;$mins=120;$divider=72000}
    }
    Switch ($Ressource){
        'CPU'
        {
            $cpuStats=Get-Stat -Entity (get-vm $VM -Server $Server) -Stat cpu.ready.summation,cpu.usage.average -start (get-date).adddays($days) -finish (get-date) -interval $mins -instance "" -ea silentlycontinue | Group-Object entity
            $cpuStatsOutput=@()
            ForEach ($group in $cpuStats)
            {
                $temp= "" | select-Object Name, "Avg. CPU Ready-%", "Avg. CPU Ready-ms", "Avg. CPU Utilization"
                $temp.name=$group.name
                $temp."Avg. CPU Ready-%"= “{0:n2}” -f (($group.group | Where-Object{ $_.MetricID -eq "cpu.ready.summation"} | Measure-Object value -ave).average/$divider)
                $temp."Avg. CPU Ready-ms"= “{0:n0}” -f ($group.group | Where-Object{ $_.MetricID -eq "cpu.ready.summation"} | Measure-Object -Property Value -Average).Average
                $temp."Avg. CPU Utilization"= “{0:n1}” -f ($group.group | Where-Object{ $_.MetricID -eq "cpu.usage.average"} | Measure-Object value -Average).average

                $cpuStatsOutput+=$temp
            }
            if($ReportPath){
                $cpuStatsOutput | ConvertTo-Csv -NoTypeInformation | Out-File $ReportPath
            }
            if(!$ReportPath){
                $cpuStatsOutput
            }   
        }
        'Disk'
        {
        #####
        }
        'Network'
        {
        #####
        }
    }
}
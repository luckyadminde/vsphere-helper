﻿##############################################################################
#.SYNOPSIS
# Get the list of Datastoer associated with VM
#
#.DESCRIPTION
# Queries VM Object and construct a HashMap with Datastores associates
# with the VM
# Key = VM Name
# Value = List of Datastores associated
#
#.EXAMPLE
# $VmMap +=  Get-VmDatastore -Vm $vm
# Alternate option : Get-Datastore -Name "local-0" | Select $_ | Get-VM
##############################################################################
Function Get-VmDatastoreName {
       param([parameter(Mandatory=$true)][Object]$Vm)
       foreach($datastore in $vm.DatastoreIdList) {
                #$datastoreObj = Get-View -ID $datastore
                $datastoreObj = Get-Datastore -Id $datastore
                $dsNameList += , $datastoreObj
        }
       return $dsNameList
}

Function Get-AllVmDatastores {
param(
[parameter(Mandatory=$true)][String]$Server,
[parameter(Mandatory=$true)][String]$VM
)
    $vmObjects = Get-VM $VM -Server $Server
    $vmDatastoreNames = foreach ($vms in $vmObjects){Get-VmDatastoreName $vms}
    $vmDatastoresNamesFiltered = $vmDatastoreNames | Sort-Object Name -Unique
    return $vmDatastoresNamesFiltered
}
